// /* Теоретичні питання:
// 1. DOM - об'єктна модель документу, яка представляє весь зміст сторінки у вигляді об'єктів, які можна змінювати. 

// 2. InnerHTML поверне контент у HTML форматі разом з тегами та пробілами. InnerText повертає контект у вигляжі простого тексту, який записаний між тегами у HTML документі.

// 3. До елементів можна звертатися декількома способами: document.getElement* та document.querySelector*. Найуніверсальніший метод - elem.querySellectorAll(css), адже він пвертає всі елементи всередині elem, які відповідають даному CSS-селектору. Такий запис коротший і зрозуміліший при читанні.

// //ЗАВДАННЯ

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(paragraph => paragraph.style.backgroundColor = "#ff0000");
  
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionList = document.getElementById('optionsList')
console.log(optionList);
console.log(optionList.parentElement);
console.log(optionList.children);


// Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
// This is a paragraph
const newText = document.querySelector('#testParagraph');
newText.innerHTML = "This is a paragraph";
console.log(newText);

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const childrenHeader = document.getElementsByClassName('main-header');
console.log(childrenHeader[0].children);

for(let child of childrenHeader[0].children) {
    child.classList.add('nav-item');
}
console.log(childrenHeader[0].children);


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const elemSection = document.querySelectorAll('.section-title');
console.log(elemSection);

elemSection.forEach(item => item.classList.remove('section-title'));
console.log(elemSection);
