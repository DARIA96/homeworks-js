/* Теоретичні питання: 
1. Рекурсія - це коли функція викликає саму себе знову і знову, доки не досягне базової умови.

Рекурсія використовується на практиці для розв'язання складних задач, які можуть бути розкладені на більш прості підзадачі. Рекурсивні алгоритми зазвичай є більш зрозумілими та читабельними, що робить їх використанням більш привабливим. 

Наприклад, рекурсія може використовуватись для обчислення факторіалу числа. */


// Завдання
let number = getNumberFromPrompt("Enter number");

function getNumberFromPrompt(text) {
  let inputNum = prompt(text, 1);
  while ((Number(inputNum) > 0 || inputNum === null) === false) {
    inputNum = prompt("Sorry, it's not a number. Try again", inputNum);
  }
  return Number(inputNum);
}

function factorial(n) {
  return n != 1 ? n * factorial(n - 1) : 1;
}
alert(factorial(number));
