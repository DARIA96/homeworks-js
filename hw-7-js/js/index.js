/* Теоретичні питання:
1. Метод forEach - запускає функцію для кожного елемента масива. Найчастіше використовується для перебору елементів масиву. Також для циєї ж цілі використовуть цикл for..of.

2. Є 4 способи як очистити масив:
let a = [1, 2, 3]
    1) присвоїти змінній 'а' новий пустий масив. 
        а = [];
    2) задати довжину масиву 0. 
        a.length = 0;
    3) використати метод splice().
        a.splice(0, a.length);
    4) використати метод pop() у циклі while:
        while(a.length > 0) {
            a.pop();
        }
3. Так як тип даних у масиву - об'єкт, ми не можемо використати оператор typeof для перевірки типу даних. Тому було додано метод Array.isArray(a) який повертає true/false.*/

// ЗАВДАННЯ

function filterBy(arr, dataType) {
  let filteredArr = arr.filter(item => typeof item !== dataType);

  return filteredArr;
}

let arr = ["hello", "world", 23, "23", null];
let filtered = filterBy(arr, "number");

console.log(arr);
console.log(filtered);
