function cloneObject(obj) {
    if (obj === null || typeof obj !== "object") {
      return obj;
    }
  
    let clone = {};
  
    if (Array.isArray(obj)) {
      clone = [];
    }
  
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        clone[prop] = cloneObject(obj[prop]);
      }
    }
  
    return clone;
  }

