const body = document.querySelector('body');
const btnChangeTheme = document.querySelector('.changeTheme');
const icon = document.querySelector('.fa-sun');
const asideLinks = document.querySelectorAll('.aside-link');
const footer = document.querySelector('.footer');
const text = document.querySelector('.main-text');
const logo = document.querySelector('.logo-black');
const menuList = document.querySelector('.aside-menu-list');

btnChangeTheme.addEventListener('click', (event) => {
    event.preventDefault()
    if(localStorage.getItem('theme') === 'dark') {
        localStorage.removeItem('theme');
    } else {
        localStorage.setItem('theme', 'dark')
    }
    addThemeToBody();
});

function addThemeToBody() {
    if(localStorage.getItem('theme') === 'dark') {
        body.classList.add('dark');
        asideLinks.forEach(link => link.classList.add('aside-menu-list-dk-theme'));
        footer.classList.add('footer-dk-theme');
        text.classList.add('text-dk-theme');
        logo.classList.add('logo-white');
        menuList.classList.add('menu-dk');
        icon.classList.remove('fa-moon');
        icon.classList.add('fa-sun');
    } else {
        body.classList.remove('dark');
        icon.classList.remove('fa-sun');
        icon.classList.add('fa-moon');
        footer.classList.remove('footer-dk-theme');
        text.classList.remove('text-dk-theme');
        logo.classList.remove('logo-white');
        menuList.classList.remove('menu-dk');
        asideLinks.forEach(link => link.classList.remove('aside-menu-list-dk-theme'));
    }
}

addThemeToBody();