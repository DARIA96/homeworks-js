/* Теоретичні питання 
1. Метод об'єкту - це фунція, яка записана всередині об'єкту.
2. Значення властивості об'єкта може мати будь-який існуючий в js тип даних (number, string, object, function, null, undefined, boolean).
3. Об'єкт - посилальний тип даних. Це означає, що ключі та значення об'єкту зберігаються не в змнінний, як це буває з примітивними типами, а ця змінна збергіє в собі посилання на ячейку пам'яти, де зберігаються дані про об'єкт. 
*/

// ЗАВДАННЯ
function createNewUser() {
  let firstName = prompt("Введіть ім'я:");
  let lastName = prompt("Введіть прізвище:");

  const newUser = {
    getLogin() {
      return (firstName[0] + lastName).toLowerCase();
    },
    setFirstName(newFirstName) {
      firstName = newFirstName;
    },
    setLastName(newLastName) {
      lastName = newLastName;
    },
  };

  Object.defineProperty(newUser, "firstName", {
    get() {
      return firstName;
    },
    enumerable: true,
  });

  Object.defineProperty(newUser, "lastName", {
    get() {
      return lastName;
    },
    enumerable: true,
  });

  return newUser;
}

const user = createNewUser();

console.log(user);
console.log(user.getLogin());
