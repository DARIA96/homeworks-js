/* Теоретичні питання:
1. Новий HTML тег на сторінці можна створити за допомогою методу .createElement() на батьківському елементі (наприклад document). Також можна створити текстову ноду за допомогою методу .createTextNode, яку потім необхідно прив'язати до створеного елементу через .createElement(). Але перший варіант є більш універсальним та коротшим у записі.

2. Функція element.insertAdjacentHTML(where, HTML-code) додає HTML код через JS з усіма тегами. Перший параметр приймає позицію коду, відповідно до батьквського елементу, другий параметр - сам html-код. Існує 4 варіанти позиціонування нового коду на сторінці: "beforebegin" - вставка одразу перед element , "afterbegin" - вставка на початку element як перший child, "beforeend" - вставка в кінці element як останній child, "afterend" - вставка одразу після element.

3. Елемент можна видалити примінивши до нього метод .remove() або .removeChild(exemple) викликаний на батьківському елементі.
*/

//ЗАВДАННЯ

function createList(array, parent = document.body) {
  const list = document.createElement("ul");
  parent.appendChild(list);

  array.forEach((item) => {
    const listItem = document.createElement("li");
    list.appendChild(listItem);

    if (Array.isArray(item)) {
      createList(item, listItem);
    } else {
      listItem.innerText = item;
    }
  });
}

createList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);

let seconds = 3;
const timer = setInterval(() => {
  seconds--;
  console.log(seconds);
}, 1000);

setTimeout(() => {
  clearInterval(timer);
  document.body.innerText = '';
}, 3000);
