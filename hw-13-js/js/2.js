const images = document.querySelectorAll('.image-to-show');
const stopButton = document.querySelector('.stopBtn');
const goButton = document.querySelector('.goBtn');


let currentIndex = 0;
let intervalId;


function slider() {
    for(let i = 0; i < images.length; i++) {
        images[i].style.display = 'none';
    }
    images[currentIndex].style.display = 'block';
    currentIndex++;

    if(currentIndex >= images.length) currentIndex = 0;
}

function start() {
    slider();
    intervalId = setInterval(slider, 3000);
}

function stop() {
    clearInterval(intervalId);
}

stopButton = addEventListener('click', stop);
goButton = addEventListener('click', start)

start()