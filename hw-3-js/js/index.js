/* Теоретичні питання:
1.Цикли у програмуванні потрібні тоді, коли треба виконати однотипну дію декілька разів.
2. Цикл while використовувала тоді, коли треба виконати код, поки умова цього циклу правдива(Напр: виводила i доки i < 3);
Цикл do...while спочатку виконує тіло циклу, а лише потім перевіряє умову, і поки його значення true - продожувати виконувати тіло циклу знову і знову;
Цикл for використовується найчастіше. В for маємо обов'язково задати початок циклу(let i = 0), умову(i < 3) та крок(i++) і вже потім виконувати тіло циклу. 
3. Неявне перетворення типів - спосіб перетворення одного типу даних в інший, який скрипт виконує автоматично.
    Наприклад:
  let myName = "Dasha";
  let myAge = 26;
  let myData = myName + myAge;
  console.log(myData);
  Dasha26
  typeof myData;
  'string'   
В даному пркладі поєднуються два типи даних: string і number. Якщо хоч одне із значень є string, то решта типів також стає string. 
Це перетворення неявне, тому що нам не треба було явно вказувати скрипту перетворити один тип даних на інший.
Явне перетворення типів - коли ми явно вказуємо скрипту, що треьа один тип даних перетворити на інший. 
Наприклад: 
alert(Boolean(1)) // true;
alert(Boolean(0)) // false;
alert(Number('123')) // 123;
alert(String(123)) // '123'
*/



//ЗАВДАННЯ 1

let number = Number(prompt("Enter number"));

while (!Number.isInteger(number) || Number.isNaN(number)) {
  number = Number(prompt("Enter integer number"));
}
for (i = 5; i <= number; i++) {
  if (i % 5 == 0 && Number.isInteger(number)) {
    console.log(i);
  }
}
if (number < 5) {
  console.log("Sorry, no numbers");
}

// ЗАВДАННЯ 2 (підвищеної складності)

let m = Number(prompt("Enter first number"));
let n = Number(prompt("Enter second number"));

if (m >= n) {
  alert("Second number must be grater than the first, try again.");
}
if(m < 2){
    m = 2;
} 

nextPrime:
for (let i = m; i <= n; i++) { 
    for (let j = 2; j < i; j++) {
        if(i % j == 0) {
           continue nextPrime;
        }
    }
    console.log(i);
}
