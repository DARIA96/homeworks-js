// Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.

const F0 = 0;
const F1 = 1;
const n = getNumberFromPrompt("Введіть число, щоб побачити результат обчислення.");

function getNumberFromPrompt(text) {
  let inputNum = prompt(text, 0);
  while ((Number(inputNum) || inputNum === null) === false) {
    inputNum = prompt("Вибачте, це не цифра. Спробуйте ще раз.", inputNum);
  }
  return Number(inputNum);
}

function fib(F0, F1, n) {
  if (n === 0) {
    return F0;
  } else if (n === 1) {
    return F1;
  } else if (n < 0) {
    return fib(F1 - F0, F0, n + 1);
  } else {
    return fib(F1, F0 + F1, n - 1);
  }
}

const result = fib(F0, F1, n);

alert(`Число Фібоначчі під номером ${n} дорівнює ${result}`);
