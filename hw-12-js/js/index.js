/* Теоретичне питання:
1. Для роботи з полями вводу одних клавіатурних подій не достатньо, адже зараз є багато інших способів ввести щось у поле: через розпізнавання мовлення, копіювання/вставка тексту за допомогою мишки. 
*/

// ЗАВДАННЯ
const buttons = document.querySelectorAll('.btn');
let activeButton = null;

function activateButton(button) {
    buttons.forEach(btn => {
        if (btn !== button) {
            btn.classList.remove('blue');
        }
    });
    button.classList.add('blue');
    activeButton = button;
}

function handleKeyDown(event) {
    const keyCode = event.keyCode;
    const button = document.querySelector(`[data-key="${keyCode}"]`);
    if (button) {
        activateButton(button);
    }
}

document.addEventListener('keydown', handleKeyDown);