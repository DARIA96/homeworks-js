/* Теоретичні питання 
1. Функції - блоки коду, які використовуються коли треба виконати якусь задачу декілька разів в різних частинах програми. 
2. Аргумент - значення, що передається функції під час її виклику у параметри.
3. Оператор return зупиняє функцію і значення, написане біля return повертається у код, який її викликав. Викликів return може бути декілька, вони можуть стояти у будь-якому місци тіла функциї, а також можуть не мати значень, тоді return призводить до негайного виходу із функції.
*/

// ЗАВДАННЯ



let firstNumber = getNumberFromPrompt("Enter first number");
let secondNumber = getNumberFromPrompt("Enter second number");

function getNumberFromPrompt(text) {
    let inputNum = prompt(text, 0);
  while ((Number(inputNum) >= 1 || inputNum === null) === false) {
    inputNum = prompt("Sorry, it's not a number. Try again", inputNum);
  }
  inputNum = Number(inputNum);
  return inputNum;
}



let oper = prompt("Enter operation (+, -, *, /)", "");
while (oper !== "+" && oper !== "-" && oper !== "/" && oper !== "*") {
  oper = prompt("Enter operation once again");
}

function calcNumbers(num1, num2, oper) {
  switch (oper) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "/":
      return num1 / num2;
    case "*":
      return num1 * num2;
  }
  return false;
}
let result = calcNumbers(firstNumber, secondNumber, oper);
console.log(result);
