/*Теоретичні питання
1. Екранування символів необхідне тоді, коли ми хочемо зробити певні маніпуляциї з символами, які присутні у регулярних виразах і можуть видати помилку, якщо ми просто будемо намагатися наприклад, знайти цей символ у коді. В такому випадку ми маємо зробити екранування символів, аби руший JS їх правильно розпізнав. 
Наприклад: 
Треба у коді знайти крапку, яка є просто крапкою, а не спецсимволом:
    alert( "Chapter 5.1".match(/\d\.\d/) ); // 5.1 (співпадіння!)
Якщо ми шукаємо бекслеш \, це спеціальний символ як у звичайних рядках, так і в регулярних виразах, ми повинні подвоїти його (екранувати).

2. Функцію можна оголосити трьома способами:
    1) Function Declaration. Через ключове слово function. Потім назва функції і список її параметрів у круглих дужках. Після цього у фігурних дужках записуємо вже "тіло функції", тобто сам код. 
        function name(parameter1, parameter2, ... parameterN) {
        ...тіло функції...
        }
    2) Function Expression. Можемо створити функцію в середині будь-якого виразу та присвоїти якійсь змінній функцию у якості значення. 
        let sayHi = function() {
            alert( "Привіт" );
        }; 
    3) Стрілкові функції. Використовуються для простих операцій, особливо для тих, що можна записати в один рядок. Можуть мати фігурні дужки або ні.
        let func = (arg1, arg2, ..., argN) => expression;

3. Hoisting - це механізм js, коли оголошення змінної чи функції фізично переноситься на початок нашого коду, ще до виконання коду. Тобто, неважливо де змінні або функції були оголошені, вони завжди передвигаються на початок області видимості (глобальної чи локальної). Так можна робити з функціями та змінними типу var, але не з let чи const. Ми не можемо їх використовувати до їх оголошення. 'use strict' також не дозволяє використовувати змінні до їх оголошення. Це дозволяє працювати з кодом у більш передбачуваній  та логічній манері. 
*/

// ЗАВДАННЯ
function createNewUser() {
    let firstName = prompt("Введіть ім'я:");
    let lastName = prompt("Введіть прізвище:");
    let birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy:");
  
    const newUser = {
      getLogin() {
        return (firstName[0] + lastName).toLowerCase();
      },
      setFirstName(newFirstName) {
        firstName = newFirstName;
      },
      setLastName(newLastName) {
        lastName = newLastName;
      },
      getAge() {
        const today = new Date();
        const birthDate = new Date(birthday.split(".").reverse().join("-"));
        let age = today.getFullYear() - birthDate.getFullYear();
        const month = today.getMonth() - birthDate.getMonth();
        if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
          age--;
        }
        return age;
      },
      getPassword() {
        const year = birthday.split(".")[2];
        return firstName[0].toUpperCase() + lastName.toLowerCase() + year;
      },
    };
  
    Object.defineProperty(newUser, "firstName", {
      get() {
        return firstName;
      },
      enumerable: true,
    });
  
    Object.defineProperty(newUser, "lastName", {
      get() {
        return lastName;
      },
      enumerable: true,
    });
  
    Object.defineProperty(newUser, "birthday", {
      get() {
        return birthday;
      },
      enumerable: true,
    });
  
    return newUser;
  }
  
  const user = createNewUser();
  
  console.log(user);
  console.log(user.getAge());
  console.log(user.getPassword());
  