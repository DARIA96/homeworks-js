let student = {
  name: "",
  lastName: "",
};

student.name = prompt("Введіть ваше ім'я:");
student.lastName = prompt("Введіть ваше прізвище:");

let tabel = {};
while (true) {
  let subject = prompt("Введіть назву предмета:");
  if (!subject) {
    break;
  }

  let grade = prompt(`Введіть оцінку з предмета ${subject}:`);
  while (!Number(grade)) {
    grade = prompt(`Ви десь помилились, вкажіть оцінку з предмета ${subject} цифрою:`, grade);
  }
  tabel[subject] = +grade;
}
student.tabel = tabel;

console.log(student);

let badGrades = 0;
let totalGrades = 0;
let numSubjects = 0;

for (let subject in tabel) {
  numSubjects++;
  totalGrades += tabel[subject];
  if (tabel[subject] < 4) {
    badGrades++;
  }
}
student.averageGrade = totalGrades / numSubjects;

if (badGrades < 0) {
  console.log(`Студента ${student.name} ${student.lastName} переведено на наступний курс.`);
}

if (student.averageGrade > 7) {
  console.log(`Студенту ${student.name} ${student.lastName} призначено стипендію.`);
}
