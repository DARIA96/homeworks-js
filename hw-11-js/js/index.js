// івент для першого поля вводу
const togglePassword = document.querySelector("#togglePassword");
const passwordInput = document.querySelector("#password");

togglePassword.addEventListener("click", function () {
  const type = passwordInput.getAttribute("type") === "password" ? "text" : "password";
  passwordInput.setAttribute("type", type);
  togglePassword.classList.toggle("fa-eye");
  togglePassword.classList.toggle("fa-eye-slash");
});

// івент для другого поля вводу
const toggleConfirmPassword = document.querySelector("#toggleConfirmPassword");
const confirmPasswordInput = document.querySelector("#confirmPassword");

toggleConfirmPassword.addEventListener("click", function () {
  const type =
    confirmPasswordInput.getAttribute("type") === "password"
      ? "text"
      : "password";
  confirmPasswordInput.setAttribute("type", type);
  toggleConfirmPassword.classList.toggle("fa-eye-slash");
  toggleConfirmPassword.classList.toggle("fa-eye");
});

// створила текст на випадок некоректного вводу пароля
const errorText = document.createElement('span');
errorText.classList.add('error-text');
errorText.innerText = 'Потрібно ввести однакові значення';
confirmPasswordInput.insertAdjacentElement('afterend', errorText);

// валідація двух інпутів
document.querySelector('form').addEventListener('submit', (e) => {
  e.preventDefault();
  if (passwordInput.value !== confirmPasswordInput.value) {
    errorText.style.display = 'block';
    errorText.style.color = 'red';
    confirmPasswordInput.style.border = '1px solid red';
  } else {
    alert('You are welcome!');
  }
});

// можна написати ще через делегування подій, як на мене - то виглядає не так зрозуміло як код вище

// const form = document.querySelector('form');
// const togglePassword = form.querySelector('#togglePassword');
// const passwordInput = form.querySelector('#password');
// const toggleConfirmPassword = form.querySelector('#toggleConfirmPassword');
// const confirmPasswordInput = form.querySelector('#confirmPassword');
// const confirmPasswordError = form.querySelector('#confirmPasswordError');
// const submitButton = form.querySelector('#submitButton');

// const errorText = document.createElement('span');
// errorText.classList.add('error-text');
// errorText.innerText = 'Потрібно ввести однакові значення';
// confirmPasswordInput.insertAdjacentElement('afterend', errorText);

// form.addEventListener('click', function (e) {
//   if (e.target === togglePassword) {
//     const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
//     passwordInput.setAttribute('type', type);
//     togglePassword.classList.toggle('fa-eye');
//     togglePassword.classList.toggle('fa-eye-slash');
//   } else if (e.target === toggleConfirmPassword) {
//     const type = confirmPasswordInput.getAttribute('type') === 'password' ? 'text' : 'password';
//     confirmPasswordInput.setAttribute('type', type);
//     toggleConfirmPassword.classList.toggle('fa-eye-slash');
//     toggleConfirmPassword.classList.toggle('fa-eye');
//   } else if (e.target === submitButton) {
//     if (passwordInput.value !== confirmPasswordInput.value) {
//       e.preventDefault();
//       errorText.style.display = 'block';
//       errorText.style.color = 'red';
//       confirmPasswordInput.style.border = '1px solid red';
//       confirmPasswordInput.classList.add('error');
//     } else {
//       alert('You are welcome!');
//     }
//   }
// });


