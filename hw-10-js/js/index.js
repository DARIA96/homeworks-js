const tabsContainer = document.querySelector(".tabs");
const tabsTitles = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tab-content");

tabsContainer.addEventListener("click", (event) => {
  const selectedTab = event.target;
  const selectedTabData = selectedTab.getAttribute("data-tab");

  tabsTitles.forEach((title) => {
    const titleData = title.getAttribute("data-tab");
    if (titleData === selectedTabData) {
      title.classList.add("active");
    } else {
      title.classList.remove("active");
    }
  });

  tabsContent.forEach((content) => {
    const contentData = content.getAttribute("data-tab");
    if (contentData === `${selectedTabData}-content`) {
      content.classList.add("active");
    } else {
      content.classList.remove("active");
    }
  });
});
